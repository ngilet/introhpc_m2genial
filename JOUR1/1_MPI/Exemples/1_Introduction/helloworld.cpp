#include <mpi.h> 
#include <iostream>

int main(int argc, char **argv){

   MPI_Init(&argc, &argv) ; 
   MPI_Comm comm = MPI_COMM_WORLD ; 
   int nprocs, rank ; 
   
   MPI_Comm_size(comm, &nprocs) ; 
   MPI_Comm_rank(comm, &rank) ; 
   
   std::cout << "Je suis le processus " << rank << " parmis " << nprocs << " processus total. " << std::endl ; 
   
   MPI_Finalize() ; 

   return 0; 
}
