#include <iostream>
#include <vector>
#include <mpi.h>

int main(int argc, char** argv) {
    // Initialisation de MPI
    MPI_Init(&argc, &argv);

    // Nombre de processus MPI
    int nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // Rang du processus MPI actuel
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Données à envoyer depuis chaque processus MPI
    std::vector<int> senddata;
    for (int i = 0; i < rank + 1; i++) {
        senddata.push_back(i);
    }

    // Nombre de données à recevoir depuis chaque processus MPI
    std::vector<int> recvcounts(nprocs);
    std::vector<int> displs(nprocs);
    int total_count = 0;
    for (int i = 0; i < nprocs; i++) {
        // Chaque processus envoie un sous-ensemble de données de taille variable
        recvcounts[i] = i + 1;
        displs[i] = total_count;
        total_count += recvcounts[i];
    }

    // Tableau de réception pour le processus de rang 0
    std::vector<int> recvdata(total_count);

    // Appel à MPI_Gatherv pour rassembler les données
    MPI_Gatherv(senddata.data(), senddata.size(), MPI_INT,
                 recvdata.data(), recvcounts.data(), displs.data(), MPI_INT,
                 0, MPI_COMM_WORLD);

    // Affichage des données reçues par le processus de rang 0
    if (rank == 0) {
        for (int i = 0; i < total_count; i++) {
            std::cout << recvdata[i] << " ";
        }
        std::cout << std::endl;
    }

    // Finalisation de MPI
    MPI_Finalize();

    return 0;
}
