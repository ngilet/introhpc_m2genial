#include <mpi.h>
#include <iostream>
#include <vector>

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    const int data_size = 8;
    int sendbuf[data_size];
    int recvbuf[data_size];

    // Initialiser le buffer d'envoi avec le rang du processus
    for (int i = 0; i < data_size; ++i) {
        sendbuf[i] = rank*data_size+i;
    }

    // Afficher le buffer d'envoi pour chaque processus
    std::cout << "Processus " << rank << " envoie: ";
    for (int i = 0; i < data_size; ++i) {
        std::cout << sendbuf[i] << " ";
    }
    std::cout << std::endl;

    // Effectuer MPI_Alltoall
    MPI_Alltoall(sendbuf, data_size/size, MPI_INT, recvbuf, data_size/size, MPI_INT, MPI_COMM_WORLD);

    // Afficher le buffer de réception pour chaque processus
    std::cout << "Processus " << rank << " reçoit: ";
    for (int i = 0; i < data_size; ++i) {
        std::cout << recvbuf[i] << " ";
    }
    std::cout << std::endl;

    MPI_Finalize();
    return 0;
}
