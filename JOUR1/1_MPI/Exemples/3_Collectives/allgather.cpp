#include <iostream>
#include <vector>
#include <mpi.h>

int main(int argc, char** argv) {
    // Initialisation de MPI
    MPI_Init(&argc, &argv);

    // Nombre de processus MPI
    int nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // Rang du processus MPI actuel
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Données à envoyer depuis chaque processus MPI
    std::vector<int> senddata(2);
    senddata[0] = rank * 2;
    senddata[1] = rank * 2 + 1;

    // Nombre de données à recevoir depuis chaque processus MPI
    int recvcount = 2;

    // Tableau de réception pour tous les processus MPI
    std::vector<int> recvdata(nprocs * recvcount);

    // Appel à MPI_Allgather pour rassembler les données
    MPI_Allgather(senddata.data(), senddata.size(), MPI_INT,
                  recvdata.data(), recvcount, MPI_INT,
                  MPI_COMM_WORLD);

    // Affichage des données reçues par chaque processus MPI
    std::cout << "Processus " << rank << " : ";
    for (int i = 0; i < nprocs * recvcount; i++) {
        std::cout << recvdata[i] << " ";
    }
    std::cout << std::endl;

    // Finalisation de MPI
    MPI_Finalize();

    return 0;
}

