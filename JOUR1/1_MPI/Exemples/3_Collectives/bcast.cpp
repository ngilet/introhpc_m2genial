#include <mpi.h> 
#include <iostream>

int main(int argc, char** argv) {
   
   MPI_Init(&argc, &argv);
   int rank ;
   int data = 0 ; 
   
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   
   if (rank == 0) data = 10 ;
   // Diffuser la valeur de data depuis le processus de rang 0 vers tous les autres processus
   MPI_Bcast(&data, 1, MPI_INT, 0, MPI_COMM_WORLD);

   std::cout << "Processus " << rank << " , j'ai reçu la valeur " << data << std::endl ; 

   MPI_Finalize();
   
   return 0; 
}
