#include <mpi.h> 
#include <iostream>

int main(int argc, char** argv) {

   int rank, size;
    
   MPI_Init(&argc, &argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &size);
   
   const int data_size = 10; // taille du tableau de données
   
   int data[data_size] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; // tableau de données à distribuer
   int local_data_size = data_size / size; // taille de la portion de données à recevoir par chaque proc
   int local_data[local_data_size]; // portion de données à recevoir par chaque processus
 
   //Distribuer les données entre les processus avec MPI_Scatter
   MPI_Scatter(data, local_data_size, MPI_INT, local_data, local_data_size, MPI_INT, 0, MPI_COMM_WORLD) ; 
   // Afficher la portion de données reçue par chaque processus (sur chaque processus)
   std::cout << "Processus " << rank << " : ";
   for (int i = 0; i < local_data_size; i++) {
        std::cout << local_data[i] << " ";
   }
   std::cout << std::endl;

   MPI_Finalize();
   return 0; 
}
