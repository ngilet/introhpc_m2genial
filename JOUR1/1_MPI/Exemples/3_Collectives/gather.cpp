#include <mpi.h> 
#include <iostream>

int main(int argc, char** argv) {

    int rank, size;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    const int local_data_size = 2; // taille des données à envoyer par chaque processus
    int local_data[local_data_size]; // données à envoyer par chaque processus
    int data_size = local_data_size * size; // taille totale des données à rassembler
    int data[data_size]; // tableau pour stocker les données rassemblées

    // Remplir les données à envoyer par chaque processus
    for (int i = 0; i < local_data_size; i++) {
        local_data[i] = rank * local_data_size + i;
    }
    // Rassembler les données des processus avec MPI_Gather
    MPI_Gather(local_data, local_data_size, MPI_INT, data, local_data_size, MPI_INT, 0, MPI_COMM_WORLD);
    // Afficher les données rassemblées (sur le processus de rang 0)
    // Afficher les données rassemblées (sur le processus de rang 0)
    if (rank == 0) {
        std::cout << "Données rassemblées : ";
        for (int i = 0; i < data_size; i++) {
            std::cout << data[i] << " ";
        }
        std::cout << std::endl;
   }
   MPI_Finalize();
   return 0; 
}
