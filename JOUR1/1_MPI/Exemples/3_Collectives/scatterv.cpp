#include <iostream>
#include <vector>
#include <mpi.h>

int main(int argc, char** argv) {
    // Initialisation de MPI
    MPI_Init(&argc, &argv);

    // Nombre de processus MPI
    int nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // Rang du processus MPI actuel
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Données à distribuer aux processus MPI
    std::vector<int> data(10);
    for (int i = 0; i < 10; i++) {
        data[i] = i;
    }

    // Nombre de données à envoyer à chaque processus MPI
    std::vector<int> sendcounts(nprocs);
    std::vector<int> displs(nprocs);
    int total_count = 0;
    for (int i = 0; i < nprocs; i++) {
        // Chaque processus reçoit un sous-ensemble de données de taille variable
        sendcounts[i] = i + 1;
        displs[i] = total_count;
        total_count += sendcounts[i];
    }

    // Tableau de réception pour chaque processus MPI
    std::vector<int> recvdata(sendcounts[rank]);

    // Appel à MPI_Scatterv pour distribuer les données
    MPI_Scatterv(data.data(), sendcounts.data(), displs.data(), MPI_INT,
                  recvdata.data(), sendcounts[rank], MPI_INT, 0, MPI_COMM_WORLD);

    // Affichage des données reçues par chaque processus MPI
    std::cout << "Processus " << rank << " : ";
    for (int i = 0; i < sendcounts[rank]; i++) {
        std::cout << recvdata[i] << " ";
    }
    std::cout << std::endl;

    // Finalisation de MPI
    MPI_Finalize();

    return 0;
}
