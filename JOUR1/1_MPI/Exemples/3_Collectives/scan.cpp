#include <iostream>
#include <vector>
#include <mpi.h>

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int send_data, recv_data; 

    send_data = rank ;

    MPI_Scan(&send_data, &recv_data, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    std::cout << "Processus " << rank << ", j'ai reçu la valeur : " << recv_data << std::endl;
 
    MPI_Finalize();
    return 0;
}
