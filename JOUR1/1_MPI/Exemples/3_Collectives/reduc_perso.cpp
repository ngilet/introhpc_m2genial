#include <iostream>
#include <vector>
#include <cmath>
#include <mpi.h>

// Définition de l'opération de réduction personnalisée
void vec_prod_op(void* invec, void* inoutvec, int* len, MPI_Datatype* datatype) {
    int* vec = (int*) invec;
    int* vec_out = (int*) inoutvec;
    int prod = 1;
    for (int i = 0; i < *len; i++) {
        if (vec[i] != 0) {
            prod *= vec[i];
        }
    }
    *vec_out *= prod;
}

int main(int argc, char** argv) {
    // Initialisation de MPI
    MPI_Init(&argc, &argv);

    // Nombre de processus MPI
    int nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // Rang du processus MPI actuel
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Données à traiter sur chaque processus MPI
    int senddata = rank ; 

    // Création de l'opération de réduction personnalisée
    MPI_Op mult_sanszero;
    MPI_Op_create(vec_prod_op, true, &mult_sanszero);

    // Calcul du produit des éléments non nuls du vecteur distribué à l'aide de MPI_Reduce
    int prod ;
    MPI_Reduce(&senddata, &prod, 1, MPI_INT, mult_sanszero, 0, MPI_COMM_WORLD);

    // Libération de l'opération de réduction personnalisée
    MPI_Op_free(&mult_sanszero);

    // Affichage du résultat sur le processus de rang 0
    if (rank == 0) {
        std::cout << "Produit des éléments non nuls des rangs : " << prod << std::endl;
    }

    // Finalisation de MPI
    MPI_Finalize();

    return 0;
}

