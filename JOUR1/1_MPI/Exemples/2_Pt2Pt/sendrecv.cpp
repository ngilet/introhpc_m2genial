#include <iostream>
#include <mpi.h>

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int rank, size, num_proc, valeur, data;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    num_proc = (rank + 1) % 2 ; 
    data = rank + 1000 ; 
    MPI_Sendrecv(&data, 1, MPI_INT, num_proc, 0, &valeur,
                 1, MPI_INT, num_proc, 0,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    std::cout << "Je suis le processus " << rank << " , j'ai reçu " << valeur <<  " du processus " << num_proc << std::endl;

    MPI_Finalize();
    return 0;
}
