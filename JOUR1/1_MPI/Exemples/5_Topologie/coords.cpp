#include <iostream>
#include <vector>
#include <mpi.h>

int main(int argc, char** argv) {
    // Initialisation de MPI
    MPI_Init(&argc, &argv);

    // Nombre de processus MPI
    int nprocs;
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int ndims = 2; // nombre de dimensions
    int dims[ndims] = {2, 2}; // nombre de processus souhaité par direction
    int periods[ndims] = {0, 0}; // périodicité
    int reorder = 1; // autoriser le réordonnancement des rangs
    MPI_Comm cart_comm;
    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorder, &cart_comm);
    // Obtenir les coordonnées cartésiennes du processus actuel
   int coords[ndims];
   MPI_Cart_coords(cart_comm, rank, ndims, coords);
   std::cout << "Processus " << rank << " : (" << coords[0] << ", " << coords[1] << ")" << std::endl;
   MPI_Comm_free(&cart_comm);
    
    // Finalisation de MPI
    MPI_Finalize();

    return 0;
}

