#include <mpi.h>
#include <iostream>

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    int dims[2] = {0, 0};  // Dimensions de la grille
    MPI_Dims_create(size, 2, dims);

    int periods[2] = {0, 0};  // Périodicité dans chaque dimension
    MPI_Comm cart_comm;
    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &cart_comm);

    int coords[2];
    MPI_Cart_coords(cart_comm, rank, 2, coords);

    int rang_gauche, rang_droite, rang_bas, rang_haut ;
    MPI_Cart_shift(cart_comm, 0, 1, &rang_gauche, &rang_droite);  // Voisins dans la direction x 
    std::cout << "Processus " << rank << ", rang_gauche =  "  << rang_gauche 
              <<  ", rang_droite = " << rang_droite <<  std::endl;
   
    MPI_Cart_shift(cart_comm, 1, 1, &rang_bas, &rang_haut);  // Voisins dans la direction y 
    std::cout << "Processus " << rank << ", rang_bas =  "  
              << rang_bas <<  ", rang_haut = " << rang_haut <<  std::endl;
    
    MPI_Comm_free(&cart_comm);
    MPI_Finalize();
    return 0;
}
