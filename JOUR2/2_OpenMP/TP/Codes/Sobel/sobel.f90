program filtre_sobel_pgm
    use, intrinsic :: iso_fortran_env, only : stdout => output_unit
    implicit none

    integer :: i, j, row_num, col_num, max_gray, file_in_unit, ios, ierror
    integer, allocatable :: image(:,:)
    integer, dimension(3,3) :: CoefGx, CoefGy 
    character(len=128) :: filename = "pgm/casablanca.ascii" // ".pgm"
    character(len=128) :: savefile = "pgm/modified.pgm"
    
    ! ------------ Initialisation des filtres de Sobel -------------
 
    data CoefGx /-1, 0, 1, &
             -2, 0, 2, &
             -1, 0, 1/
             
    data CoefGy /-1, -2, -1, &
             0, 0, 0, &
             1, 2, 1/
              
    ! ------------ ouverture et lecture du fichier pgm ------------- 
    call get_unit (file_in_unit)

    open(unit = file_in_unit, file = filename, status = 'old', iostat = ios )

    if ( ios /= 0 ) then
       write ( *, '(a)' ) ' '
       write ( *, '(a)' ) 'PGMA_READ_TEST - Fatal error!'
       write ( *, '(a)' ) '  Could not open the file.'
       stop
    end if
 
  ! lecture de l'entête contenant le nombre de lignes et de colonnes ainsi que le niveau max de gris
  call pgma_read_header ( file_in_unit, row_num, col_num, max_gray)
  
  ! on alloue la matrice image avec les dimensions de l'image 
  allocate ( image(row_num+2,col_num+2))
  image(:,:) = 0
  call pgma_read_data ( file_in_unit, row_num, col_num, image(2:row_num+1,2:col_num+1))
  close (unit = file_in_unit)
  ! -------------- fin de lecture du fichier pgm --------------------
  
  call filtre_sobel(CoefGx,CoefGy,row_num,col_num,image)
  
  ! --------------- sauvegarde du nouveau fichier ----------------
  call pgma_write(savefile, row_num, col_num, image(2:row_num+1,2:col_num+1), ierror)

end program filtre_sobel_pgm

