program hello_world

   use omp_lib
   implicit none
   logical :: p 
   integer :: rank, nthreads
   
   !$omp parallel default(private)
      
      nthreads = omp_get_num_threads() 
      rank = omp_get_thread_num()
      p = OMP_IN_PARALLEL() 
      print *, "Hello World du thread ", rank, " sur ", nthreads
   !$omp end parallel 


end program hello_world
