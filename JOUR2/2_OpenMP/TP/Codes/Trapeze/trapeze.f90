program calculate_pi
  implicit none
  integer, parameter :: dp = kind(1.0d0)
  real(dp) :: a, b, h, pi, x, pi_exact, error 
  integer :: n, i
  integer :: t1,t2, rate
  real :: t_real, t_cpu,  t1cpu, t2cpu
  
  call system_clock(t1,rate)
  call cpu_time(t1cpu)


  ! Définir les bornes de l'intégration et le nombre de trapèzes
  a = 0.0_dp
  b = 1.0_dp
  n = 100000000

  h = (b - a) / n
  pi = 0.0_dp
  pi_exact = acos(-1.0_dp)

  ! Calcul de l'intégrale 
 
  ! A COMPLETER 
 
  ! Calculer l'erreur
  error = abs(pi - pi_exact)

  call system_clock(t2)
  t_real = real(t2 - t1) / rate
  call cpu_time(t2cpu)
  t_cpu = t2cpu - t1cpu

  print *, 'La valeur approximative de pi est      :', pi
  print *, 'La valeur exacte de pi est            :', pi_exact
  print *, 'L''erreur absolue de la valeur calculée :', error
  write(*,*) 'Temps réel de calcul : ', t_real, ' secondes'
  write(*,*) 'Temps CPU de calcul : ', t_cpu, ' secondes'


contains

  ! Fonction à intégrer
  function f(x) result(fx)
    implicit none
    real(dp), intent(in) :: x
    real(dp) :: fx

    ! A COMPLETER

  end function f

end program calculate_pi
