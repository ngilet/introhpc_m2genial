# plot_normalized.gnu

# Définir le terminal de sortie
set terminal pngcairo size 800,600 enhanced font 'Verdana,10'
set output 'perf_trapeze.png'
set xlabel 'Nombre de threads'
set ylabel 'Accélération'
set title 'Accélération obtenue sur la methode des trapezes'
set key inside top left
set grid


set table 'donnees.dat'
plot [1:10] x
unset table

# Définir la grille
set grid

# Charger les données et extraire la première valeur de y
first_y = NaN
stats 'time.dat' using 2 every ::0::0 nooutput
#first_y = 0
#stats 'time.dat' using 2 nooutput
first_y = STATS_max

# Tracer les données avec y normalisé par rapport à la première valeur de y
plot 'time.dat' using 1:(first_y/$2) with linespoints pt 7 ps 1.5 lc rgb "blue" title 'Accélération observée', \
     'donnees.dat' using 1:2 with line lc rgb "red" title 'Accélération idéale'
