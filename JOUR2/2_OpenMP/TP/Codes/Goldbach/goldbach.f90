program prog_goldbach
   implicit none 

   integer, parameter :: N = 100000
   integer :: i  
   integer :: numpair(N/2) 
   integer :: t1,t2, rate
   real :: t_real, t_cpu,  t1cpu, t2cpu
  
   call system_clock(t1,rate)
   call cpu_time(t1cpu)
   
   do i=2,N,2
      write(*,*) "Je teste pour i = ", i 
      ! A COMPLETER
      write(*,*) "Le nombre de paires est de : ", numpair(i/2) 
   enddo
   
   call system_clock(t2)
   t_real = real(t2 - t1) / rate
   call cpu_time(t2cpu)
   t_cpu = t2cpu - t1cpu
   write(*,*) 'Temps réel de calcul : ', t_real, ' secondes'
   write(*,*) 'Temps CPU de calcul : ', t_cpu, ' secondes'
   
   contains 

! Fonctions testant si n est premier
function is_prime(n) result(prime)
    integer, intent(in) :: n
    logical :: prime
    integer :: i 

    if (n < 2) then
        prime = .false.
    else if (n == 2) then
        prime = .true.
    else if (mod(n, 2) == 0) then
        prime = .false.
    else
        prime = .true.
        do i = 3, nint(sqrt(real(n))), 2
            if (mod(n, i) == 0) then
                prime = .false.
                exit
            end if
        end do
    end if

end function is_prime

function goldbach(i) result(nb_pair)
    integer, intent(in) :: i
    integer :: nb_pair, j

    nb_pair = 0

    ! COMPLETER ICI 
    

end function goldbach


endprogram prog_goldbach
