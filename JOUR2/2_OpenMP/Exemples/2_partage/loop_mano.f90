program main
    use omp_lib
    implicit none

    integer :: i, deb, fin, rang, nbthreads 
    integer, parameter :: N = 100
    integer :: a(1:N) 
    
    !$omp parallel default(private) shared(a) 
        rang = omp_get_thread_num()
        nbthreads = omp_get_num_threads() 
        deb = N*rang/nbthreads+1
        fin = N*(rang+1)/nbthreads  
        do i = deb,fin
           a(i) = 2*i 
        enddo
        
        write(*,*) "Thread ", rang, " deb = ", deb, ",  fin = ", fin
    !$omp end parallel

end program main
