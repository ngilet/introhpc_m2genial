program lastprivate
    use omp_lib
    implicit none

    integer :: i, a
    integer, parameter :: n = 100

    !$omp parallel do lastprivate(a) 
        do i=1,n
           a = i 
	end do
    !$omp end parallel do 

    write(*,*) "a =  : ", a

end program lastprivate
