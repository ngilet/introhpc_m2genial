program ex_reduc
    use omp_lib
    implicit none
    
    integer, parameter :: n = 1000
    integer :: i, total 
    integer :: a(n) 
    total = 0 
    
    do i = 1,n
    	a(i) = 1
    end do 

    !$omp parallel do default(none) shared(a) reduction(+:total)
        do i = 1,n
           total = total + a(i)
        end do
    !$omp end parallel do 

    write(*,*) "total  : ", total

end program ex_reduc
