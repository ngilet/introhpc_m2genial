program main
    use omp_lib
    implicit none

    integer :: rang, a

    !$omp parallel default(private)
        a = 91000 
        
        !$omp single
        a = -a
        !$omp end single copyprivate(a)
        
        write (*,*) "Thread : ", omp_get_thread_num(), "a = ", a 
    !$omp end parallel
   
end program main
