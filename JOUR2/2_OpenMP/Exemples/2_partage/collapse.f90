program main
    use omp_lib
    implicit none

    integer :: rang
    
    !$omp parallel default(private) 
    	rang = omp_get_thread_num() 
    	!$omp sections
    	   !$omp section
    	   write(*,*) "Thread :", rang, "est dans la section 1"
    	   !$omp section
    	   write(*,*) "Thread :", rang, "est dans la section 2"
    	   !$omp section
    	   write(*,*) "Thread :", rang, "est dans la section 3"
        !$omp end sections nowait 
    !$omp end parallel

end program main
