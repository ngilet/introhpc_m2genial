program main
    use omp_lib
    implicit none

    integer :: i,  rang
    integer, parameter :: N = 10
    integer :: a(1:N) 
    
    !$omp parallel default(private) shared(a)
        rang = omp_get_thread_num() 
	!$omp do schedule(dynamic,2)  
        do i = 1,N
           a(i) = rang 
        enddo
        !$omp end do nowait 
    !$omp end parallel
    
    write(*,*) "a = ", a(:)

end program main
