program main
    use omp_lib
    implicit none

    integer :: i,  rang
    integer, parameter :: N = 10
    integer :: a(1:N) 
    
    !$omp parallel default(private) shared(a)
        rang = omp_get_thread_num() 
	!$omp do schedule(dynamic,2) ordered 
        do i = 1,N
           !$omp ordered
           a(i) = rang 
           write(*,*) "Thread :", rang, ", iteration :", i
           !$omp end ordered
        enddo
        !$omp end do nowait 
    !$omp end parallel
   
end program main
