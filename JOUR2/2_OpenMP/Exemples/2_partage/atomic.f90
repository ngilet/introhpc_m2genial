program main
    use omp_lib
    implicit none

    integer :: rang, a
    
    a = 91000 
    
    !$omp parallel default(private) shared(a)

        !$omp atomic
        a = a + 1 
        
        write (*,*) "Thread : ", omp_get_thread_num(), "a = ", a 
    !$omp end parallel
   
end program main
