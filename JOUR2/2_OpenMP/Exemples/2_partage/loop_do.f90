program main
    use omp_lib
    implicit none

    integer :: i, i_min, i_max, rang, nbthreads 
    integer, parameter :: N = 100
    integer :: a(1:N) 
    
    !$omp parallel default(private) shared(a)
        rang = omp_get_thread_num() ; i_min = n ; i_max = 0 
	!$omp do  
        do i = 1,N
           a(i) = i 
           i_min= min(i_min,i)
           i_max= max(i_max,i) 
        enddo
        !$omp end do nowait 
        write(*,*) "Thread ", rang, " deb = ", i_min, ",  fin = ", i_max
    !$omp end parallel

end program main
