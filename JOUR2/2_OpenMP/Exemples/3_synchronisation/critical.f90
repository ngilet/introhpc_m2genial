program main
    use omp_lib
    implicit none

    integer :: rang, a, b
    
    a = 91000 
    b = 1 
    !$omp parallel default(private) shared(a,b)

        !$omp critical
        a = a + 1 
        b = b*2
        !$omp end critical 
        
    !$omp end parallel
    write (*,*) "a =  ", a, "b = ", b 
    
end program main
