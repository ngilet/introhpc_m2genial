program main
    use omp_lib
    implicit none

    integer :: i,  rang
    integer, parameter :: n = 4
    integer :: a(1:n), b(1:n) 
    do i = 1,n
       a(i) = 0
       b(i) = 0
    enddo
    
    !$omp parallel default(private) shared(a,b)
        rang = omp_get_thread_num() 
        a(rang+1) = rang 
        !$omp barrier
        b(rang+1) = a(rang+1) + a(rang+2) 
    !$omp end parallel
    
    write(*,*) "b = ", b(1:n)

end program main
