program main
    use omp_lib
    implicit none
    
    integer :: rang, nthreads, i

    !$omp parallel default(private) 
        rang = omp_get_thread_num()
        nthreads = omp_get_num_threads() 
        !$omp do 
           do i = 1,nthreads
               write(*,*) "Thread : ", rang, " se situe dans la 1ere région" 
           enddo
        !$omp end do nowait
        
        !$omp do
           do i= 1,nthreads 
              write(*,*) "Thread : ", rang, " se situe dans la 2ere région" 
           enddo
        !$omp end do 
        
    !$omp end parallel 
    
end program main
