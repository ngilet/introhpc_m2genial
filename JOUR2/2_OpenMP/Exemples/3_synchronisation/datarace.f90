program main
    use omp_lib
    implicit none

    integer :: rang, a
    
    a = 91000 
    
    !$omp parallel default(private) shared(a)
        a = a + 1 
    !$omp end parallel
    write (*,*) "a = ", a
     
end program main
