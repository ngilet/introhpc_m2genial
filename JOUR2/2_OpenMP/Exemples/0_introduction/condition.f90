program main
    use omp_lib
    implicit none

    integer :: rang, n

    n = 10
    !$omp parallel private(rang) if(n > 12) 
        write(*,*) "Thread ", omp_get_thread_num(), " dans la 1ere region"
    !$omp end parallel

    n = 13
    !$omp parallel private(rang) if(n > 12) 
        write(*,*) "Thread ", omp_get_thread_num(), " dans la 2nd region"
    !$omp end parallel

end program main

