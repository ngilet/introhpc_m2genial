PROGRAM example
  IMPLICIT NONE
  REAL(KIND=8) :: x, result

  ! Demander à l'utilisateur de saisir un nombre
  PRINT *, "Entrez un nombre :"
  READ *, x

  ! Appeler la sous-routine pour calculer la puissance de deux
  CALL power_of_two(x, result)

  ! Afficher le résultat
  PRINT *, "La puissance de deux de", x, "est", result

CONTAINS

  SUBROUTINE power_of_two(input, output)
    REAL(KIND=8), INTENT(IN) :: input
    REAL(KIND=8), INTENT(OUT) :: output

    ! Calculer la puissance de deux
    output = input ** 2
  END SUBROUTINE power_of_two

END PROGRAM example

