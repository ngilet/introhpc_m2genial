program hello_openmp
    use omp_lib
    ! Directive OpenMP pour spécifier une région parallèle
    !$omp parallel
    ! Impression du message par chaque thread parallèle
    print *, 'Hello, world!'
    ! Fin de la région parallèle
    !$omp end parallel
end program hello_openmp

