program main
    use omp_lib
    implicit none

    integer :: tp
    common /dat/tp 
    !$omp threadprivate(/dat/) 
    
    tp = 91000
    
    write(*,*) "============= Premiere region ============"
    !$omp parallel num_threads(3) copyin(/dat/)
    	tp = tp + omp_get_thread_num()
    	write(*,*) "Thread ", omp_get_thread_num(), " : tp = ", tp
    !$omp end parallel
    write(*,*) "=========================================="
    write(*,*) "============= Deuxieme region ============"
    !$omp parallel num_threads(4) 
        tp = tp + 3*omp_get_thread_num()
        
        write(*,*) "Thread ", omp_get_thread_num(), " : tp = ", tp
    !$omp end parallel 
    write(*,*) "=========================================="
    
end program main
