program main
    use omp_lib
    implicit none

    integer :: rang, a

    a = 91000

    !$omp parallel private(rang) private(a)
        rang = omp_get_thread_num()
        a = rang
        write(*,*) "Thread ", omp_get_thread_num(), " : a = ", a
    !$omp end parallel

    write(*,*) "a =  : ", a

end program main
