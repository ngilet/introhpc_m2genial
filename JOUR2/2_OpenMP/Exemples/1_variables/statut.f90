program statut

   use omp_lib
   implicit none
   integer :: x = 0  
   
   !$omp parallel
      x = x + 1  
   !$omp end parallel
    
   print *, "Valeur de x : ", x 

end program statut
