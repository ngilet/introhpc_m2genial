program main
    use omp_lib
    implicit none
    common :: c = 2000
    integer :: rang


    !$omp parallel private(rang) 
        rang = omp_get_thread_num()
        c = c + rang
        write(*,*) "Thread ", omp_get_thread_num(), " : c = ", c
    !$omp end parallel

    write(*,*) "a =  : ", c

end program main
