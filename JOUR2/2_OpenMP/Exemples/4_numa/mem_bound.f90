program NoFirstTouch
   implicit none
   integer, parameter :: n = 1200
   integer :: i,j 
   real, dimension(n) :: A,B,C
   real :: start_time, end_time, elapsed_time
   INTEGER(kind=8) :: &
     nb_periodes_initial, & ! valeur initiale du compteur de périodes d'horloge
     nb_periodes_final,   & ! valeur finale   du compteur de périodes d'horloge
     nb_periodes_max,     & ! valeur maximale du compteur d'horloge
     nb_periodes_sec,     & ! nombre de périodes d'horloge par seconde
     nb_periodes            ! nombre de périodes d'horloge du code
   REAL(kind=8) :: temps_elapsed  ! temps réel en secondes
   
   
   A(1:n) = 1.0
   B(1:n) = 1.0
   C(1:n) = 1.0
   
   CALL SYSTEM_CLOCK(COUNT_RATE=nb_periodes_sec, COUNT_MAX=nb_periodes_max)

   CALL SYSTEM_CLOCK(COUNT=nb_periodes_initial)

   call cpu_time(start_time) 

   !$omp parallel private(A,B,C)
   do i=1,n
       A(i) = A(i)*B(i) + C(i)
   enddo
   !$omp end parallel
   
   CALL SYSTEM_CLOCK(COUNT=nb_periodes_final)
   nb_periodes = nb_periodes_final - nb_periodes_initial
   IF (nb_periodes_final < nb_periodes_initial) &
          nb_periodes = nb_periodes + nb_periodes_max
   temps_elapsed   = REAL(nb_periodes) / nb_periodes_sec
   
   call cpu_time(end_time) 
   elapsed_time = end_time - start_time
   print *, "Temps CPU écoulé : ", elapsed_time
   print *, "Temps réel : ", temps_elapsed

end program NoFirstTouch
