program NoFirstTouch
   implicit none
   integer, parameter :: n = 1300
   integer :: i,j, k 
   real, dimension(n,n) :: TAB
   real :: start_time, end_time, elapsed_time
   INTEGER(kind=8) :: &
     nb_periodes_initial, & ! valeur initiale du compteur de périodes d'horloge
     nb_periodes_final,   & ! valeur finale   du compteur de périodes d'horloge
     nb_periodes_max,     & ! valeur maximale du compteur d'horloge
     nb_periodes_sec,     & ! nombre de périodes d'horloge par seconde
     nb_periodes            ! nombre de périodes d'horloge du code
   REAL(kind=8) :: temps_elapsed  ! temps réel en secondes
   CALL SYSTEM_CLOCK(COUNT_RATE=nb_periodes_sec, COUNT_MAX=nb_periodes_max)

   CALL SYSTEM_CLOCK(COUNT=nb_periodes_initial) 
   call cpu_time(start_time)

   do k=1,8000
   TAB(1:n,1:n) = 1.0
   
   !$omp parallel 
   !$omp do schedule(static)
   do j=1,n
      do i=1,n
         TAB(i,j) = TAB(i,j)+i+j
      enddo
   enddo
   !$omp end do 
   !$omp end parallel
   enddo 
   
   CALL SYSTEM_CLOCK(COUNT=nb_periodes_final)
   nb_periodes = nb_periodes_final - nb_periodes_initial
   IF (nb_periodes_final < nb_periodes_initial) &
          nb_periodes = nb_periodes + nb_periodes_max
   temps_elapsed   = REAL(nb_periodes) / nb_periodes_sec
   call cpu_time(end_time) 

   elapsed_time = end_time - start_time
   print *, "Temps CPU écoulé : ", elapsed_time
   print *, "Temps réel : ", temps_elapsed
   
end program NoFirstTouch
