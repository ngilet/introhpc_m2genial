program simd_vector_reduction
    use omp_lib
    implicit none

    integer, parameter :: n = 1000
    integer :: i, somme
    integer :: a(n)

    a(:) = 1     
    somme = 0 
    
    !$omp parallel do simd reduction(+:somme)
    do i = 1, n
        somme = somme+a(i)
    end do
    !$end parallel 
    
    write(*,*) "La somme total vaut : ", somme

end program simd_vector_reduction
