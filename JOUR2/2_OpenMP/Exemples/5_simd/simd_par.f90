program simd_vector_addition
    use omp_lib
    implicit none

    integer, parameter :: n = 300000
    integer :: i
    real(8) :: a(n), b(n), c(n)

    ! Initialiser les vecteurs a et b
    do i = 1, n
        a(i) = real(i, 8)
        b(i) = real(i, 8) * 2.0
    end do

    ! Effectuer l'addition des vecteurs a et b en utilisant OpenMP SIMD
    !$omp parallel do simd
    do i = 1, n
        c(i) = a(i) + b(i)
    end do

    ! Vérifier le résultat
    do i = 1, n
        if (c(i) /= real(3.0 * i, 8)) then
            print *, "Erreur dans l'addition des vecteurs a et b"
            stop
        end if
    end do

    print *, "Addition des vecteurs a et b réussie"

end program simd_vector_addition
