#include <iostream>
#include <omp.h>
#include <chrono>

// Fonction SAXPY
void saxpy(int n, float a, float* x, float* y, float* result) {
#pragma omp target data map(to: x[0:n], y[0:n], a) map(from: result[0:n])
    {
        #pragma omp target teams distribute parallel for
        for (int i = 0; i < n; ++i) {
            result[i] = a * x[i] + y[i];
        }
    }
}

int main() {
    // Taille des vecteurs
    int n = 1e9;
    float a = 2.0f;

    // Allocation de mémoire pour les vecteurs
    float* x = new float[n];
    float* y = new float[n];
    float* result = new float[n];

    // Initialisation des vecteurs
    for (int i = 0; i < n; ++i) {
        x[i] = static_cast<float>(1);
        y[i] = static_cast<float>(2);
    }

    auto start_time = std::chrono::high_resolution_clock::now();
    saxpy(n, a, x, y, result);
    auto end_time = std::chrono::high_resolution_clock::now();
    
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Temps d'exécution du saxpy avec OpenMP : " << duration.count()/1e6 << " secondes" << std::endl;


    // Libération de la mémoire
    delete[] x;
    delete[] y;
    delete[] result;

    return 0;
}

