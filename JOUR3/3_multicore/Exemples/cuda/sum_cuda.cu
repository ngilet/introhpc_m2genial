#include <stdio.h>
#include <stdlib.h>

__global__ void add_one(int *A, int N) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < N) {
        A[i] += 1;
    }
}

int main() {
    int N = 10;
    int *A_host, *A_device;
    size_t size = N * sizeof(int);

    // Allocation de la mémoire sur l'hôte
    A_host = (int*)malloc(size);

    // Initialisation du vecteur A
    for (int i = 0; i < N; i++) {
        A_host[i] = 1;
    }

    // Allocation de la mémoire sur le device
    cudaMalloc(&A_device, size);

    // Copie du vecteur A depuis l'hôte vers le device
    cudaMemcpy(A_device, A_host, size, cudaMemcpyHostToDevice);

    // Lancement du kernel
    int threads_per_block = 256;
    int blocks_per_grid = (N + threads_per_block - 1) / threads_per_block;
    add_one<<<blocks_per_grid, threads_per_block>>>(A_device, N);

    // Copie du vecteur A depuis le device vers l'hôte
    cudaMemcpy(A_host, A_device, size, cudaMemcpyDeviceToHost);

    // Affichage du résultat
    for (int i = 0; i < N; i++) {
        printf("%d ", A_host[i]);
    }
    printf("\n");

    // Libération de la mémoire
    free(A_host);
    cudaFree(A_device);

    return 0;
}
