#include <stdio.h>
#include <stdlib.h>

__global__ void hello()
{
  printf("Je suis le thread %d dans le block %d\n",
         threadIdx.x, blockIdx.x);
}


int main(int argc,char **argv)
{

  unsigned int gridSize  = argc > 1 ? atoi(argv[1]) : 1;
  unsigned int blockSize = argc > 2 ? atoi(argv[2]) : 16;

  hello<<<gridSize, blockSize>>>();
  
  cudaDeviceSynchronize();
  
  printf("That's all!\n");
  
  return EXIT_SUCCESS;
}
