#include <stdio.h>
#include <stdlib.h>

__global__ void add_one(int *A, int N) {
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < N) {
        A[i] += 1;
    }
}

int main() {
    int N = 10;
    int *A;
    size_t size = N * sizeof(int);

    // Allocation de la mémoire unifiée
    cudaMallocManaged(&A, size);

    // Initialisation du vecteur A
    for (int i = 0; i < N; i++) {
        A[i] = i;
    }

    // Lancement du kernel
    int threads_per_block = 256;
    int blocks_per_grid = (N + threads_per_block - 1) / threads_per_block;
    add_one<<<blocks_per_grid, threads_per_block>>>(A, N);

    // Attente de la fin du kernel
    cudaDeviceSynchronize();

    // Affichage du résultat
    for (int i = 0; i < N; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");

    // Libération de la mémoire
    cudaFree(A);

    return 0;
}
