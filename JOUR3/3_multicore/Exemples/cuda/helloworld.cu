#include <stdio.h>
#include <cuda.h>

__global__ void hello()
{
   printf("Hello depuis le GPU!\n") ;
}

int main()
{
   printf("Hello depuis le CPU!\n") ;
   hello<<<1,5>>>() ;
   //cudaDeviceSynchronize() ;
   return 0;
}
