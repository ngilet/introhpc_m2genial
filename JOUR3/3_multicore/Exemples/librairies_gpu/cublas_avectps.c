#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>

int main() {
    // Allouer de la mémoire pour les matrices A, B et C sur le GPU
    const int M = 10000;
    const int N = 10000;
    const int K = 10000;
    double *d_A, *d_B, *d_C;
    double alpha = 1.0d ;
    double beta = 0.0d ;
    cudaMallocManaged((void **) &d_A, M * K * sizeof(double), cudaMemAttachGlobal);
    cudaMallocManaged((void **) &d_B, K * N * sizeof(double), cudaMemAttachGlobal);
    cudaMallocManaged((void **) &d_C, M * N * sizeof(double), cudaMemAttachGlobal);

    // Initialiser les matrices A et B avec des valeurs arbitraires
    for (int i = 0; i < M * K; i++) {
        d_A[i] = (double) rand()/ RAND_MAX;
    }
    for (int i = 0; i < K * N; i++) {
        d_B[i] = (double) rand()/RAND_MAX;
    }

    // Initialiser la matrice C à zéro
    for (int i = 0; i < M * N; i++) {
        d_C[i] = 0.0d;
    }

    struct timespec start, end;
    double elapsed_time;

    clock_gettime(CLOCK_MONOTONIC, &start);
    // Créer un contexte cuBLAS et une poignée de poignée de matrice
    cublasHandle_t handle;
    cublasCreate(&handle);

    // COMPLETER ICI L'APPEL AU SOUS-PROGRAMME DE LA LIBRAIRIE cuBLAS 
    

    cublasDestroy(handle);
    cudaDeviceSynchronize();
    clock_gettime(CLOCK_MONOTONIC, &end);
    elapsed_time = (end.tv_sec - start.tv_sec) + (double) (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    // Afficher le temps d'exécution
    printf("Temps d'exécution en GPU : %f secondes\n", elapsed_time);
    
    // Libérer la mémoire allouée pour les matrices A, B et C sur le GPU
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    return 0;
}

