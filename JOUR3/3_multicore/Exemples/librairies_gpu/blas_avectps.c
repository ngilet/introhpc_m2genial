#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cblas.h>

int main(void) {
    // Définir les dimensions des matrices
    const int m = 2000;
    const int n = 2000;
    const int k = 2000;
    double alpha = 1.0d ; 
    double beta = 0.0d ; 
    
    // Allouer de la mémoire pour les matrices A, B et C
    double *A = malloc(m * k * sizeof(double));
    double *B = malloc(k * n * sizeof(double));
    double *C = malloc(m * n * sizeof(double));

    // Initialiser les matrices A et B avec des nombres aléatoires
    for (int i = 0; i < m * k; i++) {
        A[i] = (double) rand() / RAND_MAX;
    }
    for (int i = 0; i < k * n; i++) {
        B[i] = (double) rand() / RAND_MAX;
    }

    // Initialiser la matrice C à zéro
    for (int i = 0; i < m * n; i++) {
        C[i] = 0.0;
    }

    struct timespec start, end;
    double elapsed_time;

    clock_gettime(CLOCK_MONOTONIC, &start);
    
    // COMPLETER ICI AVEC UN APPEL A UN SOUS-PROGRAMME DE LA LIBRAIRIE BLAS 

    clock_gettime(CLOCK_MONOTONIC, &end);

    elapsed_time = (end.tv_sec - start.tv_sec) + (double) (end.tv_nsec - start.tv_nsec) / 1000000000.0;

    printf("Temps d'exécution en CPU : %f secondes\n", elapsed_time);

    // Libérer la mémoire allouée pour les matrices A, B et C
    free(A);
    free(B);
    free(C);

    return 0;
}

