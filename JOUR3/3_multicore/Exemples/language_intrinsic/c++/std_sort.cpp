#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <execution>

int main() {
  // Définir la taille du vecteur
  int N = 1e8;

  // Créer un vecteur de taille N et le remplir avec des nombres aléatoires
  std::vector<int> v(N);
  for (int i = 0; i < N; i++) {
    v[i] = rand() % 100;  // Générer un nombre aléatoire entre 0 et 99
  }

  // Mesurer le temps d'exécution de la partie de tri
  auto start_time = std::chrono::high_resolution_clock::now();
  std::sort(std::execution::par,v.begin(), v.end());
  auto end_time = std::chrono::high_resolution_clock::now();

  // Calculer la durée d'exécution en microsecondes
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
  std::cout << "Temps d'exécution du tri : " << duration.count()/1e6 << " secondes" << std::endl;

  return 0;
}

