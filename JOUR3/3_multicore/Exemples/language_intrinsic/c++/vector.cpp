#include <iostream>
#include <vector>
#include <algorithm> 

int main() {

    std::vector<int> vec = {2, 1, 4, 3, 5};

    // Afficher le vecteur avant le tri
    std::cout << "Vecteur avant le tri: ";
    for (int val : vec) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    // Trier le vecteur
    std::sort(vec.begin(), vec.end());

    // Afficher le vecteur après le tri
    std::cout << "Vecteur après le tri: ";
    for (int val : vec) {
        std::cout << val << " ";
    }
    std::cout << std::endl;

    return 0;
}
