#include <iostream>
#include <vector>
#include <algorithm> // Pour std::transform
#include <execution> // Pour std::execution::par
#include <chrono>

int main() {
    // Définir le scalaire a
    float a = 2.0f;
    int N = 1e9 ;

    // Définir les vecteurs X et Y
    std::vector<float> X(N) ;
    std::vector<float> Y(N)  ;

    for (int i = 0; i < N; i++) {
        X[i] = rand() % 100;
        Y[i] = rand() % 100;
    }


   auto start_time = std::chrono::high_resolution_clock::now();
   
    // Appliquer l'opération SAXPY en utilisant std::transform avec std::execution::par
   
   auto end_time = std::chrono::high_resolution_clock::now();
   auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
   std::cout << "Temps d'exécution du saxpy : " << duration.count()/1e6 << " secondes" << std::endl;
   return 0;
}
