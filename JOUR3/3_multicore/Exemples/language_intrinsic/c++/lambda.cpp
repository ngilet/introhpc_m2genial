#include <iostream>

int main() {
    int x = 10;
    int y = 20;

    auto somme = [&](int a) {
        return x + y + a;
    };

    std::cout << "somme(5): " << somme(5) << std::endl; 

    return 0;
}

