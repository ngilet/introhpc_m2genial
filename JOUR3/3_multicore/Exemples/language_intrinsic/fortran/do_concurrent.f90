program main
    use, intrinsic :: iso_fortran_env, only : wp => real64
    implicit none

    integer, parameter :: n = 1e8
    integer, parameter :: m = 100
    real(wp) :: a(n), b(n), c(n)
    integer :: i, start, finish, rate, j 
    real(wp) :: elapsed_time 
    
    ! Initialisation des tableaux a et b
    a = 1.0_wp
    b = 2.0_wp
    
    call system_clock(start, count_rate=rate) 
    do concurrent (i = 1:n)
        c(i) = a(i) + b(i)
        do j = 1,m
           c(i) = 2_wp*c(i) 
        end do 
    end do
    call system_clock(finish, count_rate=rate) 
    elapsed_time = real(finish-start,wp) / real(rate,wp) 
    print *, 'Temps écoulé avec do concurrent : ', elapsed_time, ' secondes'
    
    call system_clock(start, count_rate=rate) 
    do i=1,n
        c(i) = a(i) + b(i)
        do j = 1,m
            c(i) = 2_wp*c(i) 
        end do
    end do
    call system_clock(finish, count_rate=rate) 
    elapsed_time = real(finish-start,wp) / real(rate,wp) 
    print *, 'Temps écoulé sans do concurrent : ', elapsed_time, ' secondes'
    
   

end program main

