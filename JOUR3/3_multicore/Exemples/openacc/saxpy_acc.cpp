#include <iostream>
#include <vector>
#include <openacc.h>
#include <chrono>

int main() {

    float a = 2.0f;
    size_t N = 1e9 ;
    std::vector<float> X(N,1) ;
    std::vector<float> Y(N,2) ;

    auto start_time = std::chrono::high_resolution_clock::now();
    // Appliquer l'opération SAXPY en utilisant OpenACC
    #pragma acc kernels
    for (size_t i = 0; i < N; ++i) {
        Y[i] = a * X[i] + Y[i];
    }
    auto end_time = std::chrono::high_resolution_clock::now();

    // Calculer la durée d'exécution en microsecondes
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Temps d'exécution du saxpy avec OpenACC : " << duration.count()/1e6 << " secondes" << std::endl;
    return 0;
}
