#include <iostream>
#include <vector>
#include <openacc.h> 
#include <chrono>

int main() {

    float a = 2.0f;
    size_t N = 1e8 ; 
    std::vector<float> X(N,1) ;
    std::vector<float> Y(N,2) ;
    std::vector<float> Z(N,0) ;
    auto start_time = std::chrono::high_resolution_clock::now();

    #pragma acc parallel loop num_gangs(32) vector_length(128) copyin(X[1:N]) copy(Y[1:N])
    for (size_t i = 0; i < N; ++i) {
        Y[i] = a * X[i] + Y[i];
    }
    auto end_time = std::chrono::high_resolution_clock::now();
    
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    std::cout << "Temps d'exécution du saxpy avec OpenACC : " << duration.count()/1e6 << " secondes" << std::endl;
    return 0;
}

